---
# Filename: access_like.yml
# Location: /usr/share/bgscripts/inc/access_like.yml
# Author: bgstack15
# Startdate: 2018-02-01 15:00
# Title: Playbook that Sets Access Like a User for a Different User
# Purpose: To make it easy to set up similar user access
# History:
#    2018-02-02 Add sssd support
#    2018-02-09 Add basic sudoers checking
#    2018-05-09 Enable domain users to be added to local groups through the look-alike process
#    2018-06-21 Deprecated by access_like.sh
# Usage:
#    ansible-playbook -b /usr/share/bgscripts/inc/like_access.yml -l example1,example2 -e 'thisuser=newuser' -e 'likeuser=olduser' -v
# Reference:
# Improve:
# Documentation:
#  This playbook performs several major functions:
#    Learn if users are local or domain
#    If both local, set up local group memberships to be identical, except for user private groups
#    If ssh uses AllowUsers, make thisuser match likeuser
- name: Setup Access Like
  hosts: all
  vars:
    sshd_config_file: /etc/ssh/sshd_config
    sssd_conf_file: /etc/sssd/sssd.conf
    group_file: /etc/group
    sudoers_file: /etc/sudoers
    sudoers_dir: /etc/sudoers.d
  tasks:
  - set_fact:
      likeuser_is_local:  False
      likeuser_is_domain: False
      thisuser_is_local:  False
      thisuser_is_domain: False

  - name: learn if users are local or domain
    shell: warn=no getent passwd -s {{ item[1] }} {{ item[0] }} 1>/dev/null && echo "YES" || echo "no"
    changed_when: false
    with_nested:
    - [ "{{ likeuser }}", "{{ thisuser }}" ]
    - [ 'sss', 'files' ]
    register: islocalusers

  - set_fact:
      likeuser_is_domain: True
    when: 'item.stdout == "YES"'
    with_items:
    - "{{ islocalusers.results[0] }}"

  - set_fact:
      likeuser_is_local: True
    when: 'item.stdout == "YES"'
    with_items:
    - "{{ islocalusers.results[1] }}"

  - set_fact:
      thisuser_is_domain: True
    when: 'item.stdout == "YES"'
    with_items:
    - "{{ islocalusers.results[2] }}"

  - set_fact:
      thisuser_is_local: True
    when: 'item.stdout == "YES"'
    with_items:
    - "{{ islocalusers.results[3] }}"

  # Now these variables are defined as a boolean
  # likeuser_is_local
  # likeuser_is_domain
  # thisuser_is_local
  # thisuser_is_domain

# LOCAL GROUPS
  - name: learn groups of local likeuser excluding user private group
    #shell: warn=no id -nG {{ likeuser }} | tr '[[:space:]]' '\n' | xargs -n1 -I[] grep -E "^[]:" "{{ group_file }}" 2>/dev/null | awk -F':' '!/:$/{print $1}'
    shell: warn=no awk -F':' '/:.*\<{{ likeuser }}\>/{print $1;}' "{{ group_file }}" 2>/dev/null | cat
    register: thesegroups
    changed_when: false

  - name: learn primary group of first user
    shell: warn=no id -ng {{ likeuser }}
    register: this_primary_group
    changed_when: false
    when:
    - 'likeuser_is_domain or likeuser_is_local'

  - name: add thisuser to thesegroups
    user:
      name: "{{ thisuser }}"
      append: yes
      groups: "{{ thesegroups.stdout_lines }}"
# 2018-05-09 do this even when the user is domain, but it a member of local groups
#    when:
#    - 'thisuser_is_local'

  - name: add thisuser to this_primary_group, if not user private group
    user:
      name: "{{ thisuser }}"
      group: "{{ this_primary_group.stdout }}"
    when:
    - 'this_primary_group.stdout is defined and this_primary_group.stdout not in likeuser'
    - 'thisuser_is_local'

  - name: set thisuser to user private group, if user private group
    user:
      name: "{{ thisuser }}"
      group: "{{ thisuser }}"
    when:
    - 'this_primary_group.stdout is defined and this_primary_group.stdout in likeuser'
    - 'thisuser_is_local'

# SSH and SSSD
# these are checked at the same time because they each need the helper script
  - name: learn if ssh uses AllowUsers
    shell: grep -qiE "^\s*AllowUsers" "{{ sshd_config_file }}" && echo YES || echo no
    register: ssh_uses_allowusers
    ignore_errors: yes
    changed_when: false

  - name: learn if sssd uses simple_allow_users
    shell: grep -qiE "^\s*simple_allow_users" "{{ sssd_conf_file }}" && echo YES || echo no
    register: sssd_uses_simple_allow_users
    ignore_errors: yes
    changed_when: false

  - name: learn if likeuser can ssh
    shell: grep -qiE '^\s*AllowUsers.*\<{{ likeuser }}\>' "{{ sshd_config_file }}" && echo YES || echo no
    register: likeuser_can_ssh
    changed_when: false
    when: '"YES" in ssh_uses_allowusers.stdout'

  - name: learn if thisuser can already ssh
    shell: grep -qiE '^\s*AllowUsers.*\<{{ thisuser }}\>' "{{ sshd_config_file }}" && echo YES || echo no
    register: thisuser_can_ssh
    changed_when: false
    when: '"YES" in ssh_uses_allowusers.stdout'

  - name: learn if likeuser can sssd
    shell: grep -qiE '^\s*simple_allow_users.*\<{{ likeuser }}\>' "{{ sssd_conf_file }}" && echo YES || echo no
    register: likeuser_can_sssd
    changed_when: false
    when: '"YES" in sssd_uses_simple_allow_users.stdout'

  - name: learn if thisuser can already sssd
    shell: grep -qiE '^\s*simple_allow_users.*\<{{ thisuser }}\>' "{{ sssd_conf_file }}" && echo YES || echo no
    register: thisuser_can_sssd
    changed_when: false
    when: '"YES" in sssd_uses_simple_allow_users.stdout'

  - name: deploy helper script, if likeuser can ssh or sssd but thisuser cannot
    copy:
      src: "/home/bgstack15/{{ item }}"
      dest: "/tmp/{{ item }}"
      mode: 0644
      owner: root
      group: root
    changed_when: false
    with_items:
    - modconf.py
    - uvlib.py
    - bgs.py
    when:
    - '(likeuser_can_ssh.stdout is defined and "YES" in likeuser_can_ssh.stdout and thisuser_can_ssh.stdout is defined and "no" in thisuser_can_ssh.stdout) or (likeuser_can_sssd.stdout is defined and "YES" in likeuser_can_sssd.stdout and thisuser_can_sssd.stdout is defined and "no" in thisuser_can_sssd.stdout)'

# SSH
  - name: add thisuser to ssh allowusers, if likeuser can ssh but thisuser cannot
    shell: /usr/bin/python2 /tmp/modconf.py -a "{{ sshd_config_file }}" --itemdelim " " --variabledelim " " add AllowUsers "{{ thisuser }}"
    args:
      chdir: /tmp
    notify: reload sshd
    when:
    - 'likeuser_can_ssh.stdout is defined and "YES" in likeuser_can_ssh.stdout'
    - 'thisuser_can_ssh.stdout is defined and "no" in thisuser_can_ssh.stdout'

# SSSD
  - name: add thisuser to sssd simple_allow_users, if likeuser can sssd but thisuser cannot
    shell: /usr/bin/python2 /tmp/modconf.py -a "{{ sssd_conf_file }}" --itemdelim ", " --variabledelim " " add simple_allow_users "{{ thisuser }}"
    args:
      chdir: /tmp
    notify: reload sssd
    when:
    - 'likeuser_can_sssd.stdout is defined and "YES" in likeuser_can_sssd.stdout'
    - 'thisuser_can_sssd.stdout is defined and "no" in thisuser_can_sssd.stdout'

# SUDOERS
  - name: learn if likeuser is in sudoers
    shell: warn=no grep -rE '\<{{ likeuser }}\>' "{{ sudoers_file }}" "{{ sudoers_dir }}" || true
    ignore_errors: yes
    changed_when: false
    register: in_sudoers

  - name: Check sudoers on these hosts
    debug:
      msg: "{{ ansible_nodename }} {{ item }}"
    with_items: "{{ in_sudoers.stdout_lines }}"
    when: 'likeuser in in_sudoers.stdout'

# CLEANUP
  - name: clean helper scripts
    file:
      path: "/tmp/{{ item }}"
      state: absent
    changed_when: false
    ignore_errors: true
    with_items:
    - modconf.py
    - uvlib.py
    - bgs.py

  handlers:
  - name: reload sshd
    service:
      name: sshd
      state: reloaded

  - name: reload sssd
    service:
      name: sssd
      state: reloaded
