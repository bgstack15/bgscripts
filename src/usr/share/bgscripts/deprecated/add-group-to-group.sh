#!/bin/sh
# Filename: add-group-to-group.sh
# Location: 
# Author: bgstack15@gmail.com
# Startdate: 2018-11-20 16:07:50
# Title: Add Group to Group
# Purpose: Make it easy to add users of a group to another group
# Package: 
# History: 
# Usage: 
# Reference: ftemplate.sh 2018-11-14a; framework.sh 2018-10-30a
# Improve:
# ideas for add-group-to-group.sh
#    add-group-to-group --target 'aspadmin' config_management
#    will take the 15 users listed in config_management, and add them to aspadmin. Record in /var/lib/agtg.db the users this process added.
#    add-group-to-group --remove --target 'aspadmin' 'config_management' --force # will do it anyways, even if they were not added because of this group
#    add-group-to-group --remove --target 'aspadmin' 'config_management' 'group2' 'group3'
#    db contents:
#    metadata,dbversion:1,
#    date:$( date +%S );from:config_management;to:aspadmin;action:added;users:gvirden,mwellburn,sillyname3

fiversion="2018-11-14a"
addgrouptogroupversion="2018-11-20a"

usage() {
   ${PAGER:-/usr/bin/less -F} >&2 <<ENDUSAGE
usage: add-group-to-group.sh [-duV] [-c conffile]
version ${addgrouptogroupversion}
 -d debug   Show debugging info, including parsed variables.
 -u usage   Show this usage block.
 -V version Show script version number.
 -c conf    Read in this config file.
Return values:
 0 Normal
 1 Help or version info displayed
 2 Count or type of flaglessvals is incorrect
 3 Incorrect OS type
 4 Unable to find dependency
 5 Not run as root or sudo
ENDUSAGE
}

# DEFINE FUNCTIONS

workflow_add() {
   # call: workflow_add "${AGTG_TARGET_GROUP}" "${AGTG_this_group}" "${AGTG_ETC_GROUP}"
   debuglev 9 && ferror "workflow_add $@"

   ___wa_target_group="${1}"
   ___wa_this_group="${2}"
   ___wa_etc_group="${3}"

   for ___wa_user in $( get_users_in_group "${___wa_this_group}" ) ;
   do

      # check if user is in the group
      if get_users_in_group "${___wa_target_group}" | grep -xqE "${___wa_user}" ;
      then
         :
      else
         # not in the group, so add it
         debuglev 2 && ferror "add user ${___wa_user} to ${___wa_target_group}"
         gpasswd -a "${___wa_user}" "${___wa_target_group}" 2>&1 | grep -vE 'Adding user.*to group'
      fi

   done

}

workflow_remove() {
   debuglev 9 && ferror "workflow_remove $@"
}

get_users_in_group() {
   # call: get_users_in_group "groupname"
   # output: one username per line, of users from the group
   ___gufg_group="${1}"
   getent group "${___gufg_group}" 2>/dev/null | awk -F':' '{print $NF}' | xargs -d',' -n1 | sed -r -e '/^\s*$/d'
}

# DEFINE TRAPS

clean_addgrouptogroup() {
   # use at end of entire script if you need to clean up tmpfiles
   # rm -f "${tmpfile1}" "${tmpfile2}" 2>/dev/null

   # Delayed cleanup
   if test -z "${AGTG_NO_CLEAN}" ;
   then
      nohup /bin/bash <<EOF 1>/dev/null 2>&1 &
sleep "${AGTG_CLEANUP_SEC:-300}" ; /bin/rm -r "${AGTG_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

CTRLC() {
   # use with: trap "CTRLC" 2
   # useful for controlling the ctrl+c keystroke
   :
}

CTRLZ() {
   # use with: trap "CTRLZ" 18
   # useful for controlling the ctrl+z keystroke
   :
}

parseFlag() {
   flag="$1"
   hasval=0
   case ${flag} in
      # INSERT FLAGS HERE
      "d" | "debug" | "DEBUG" | "dd" ) setdebug; ferror "debug level ${debug}" ; __debug_set_by_param=1;;
      "u" | "usage" | "help" | "h" ) usage; exit 1;;
      "V" | "fcheck" | "version" ) ferror "${scriptfile} version ${addgrouptogroupversion}"; exit 1;;
      #"i" | "infile" | "inputfile" ) getval; infile1=${tempval};;
      "c" | "conf" | "conffile" | "config" ) getval; conffile="${tempval}";;
      "t" | "target" ) getval ; AGTG_TARGET_GROUP="${tempval}" ;;
      "f" | "force" ) AGTG_FORCE=yes ;;
      "nf" | "noforce" ) AGTG_FORCE=no ;;
      "a" | "add" ) AGTG_ACTION=add ;;
      "r" | "remove" ) AGTG_ACTION=remove ;;
   esac

   debuglev 10 && { test ${hasval} -eq 1 && ferror "flag: ${flag} = ${tempval}" || ferror "flag: ${flag}"; }
}

# DETERMINE LOCATION OF FRAMEWORK
f_needed=20181030
___frameworkpath="$( find $( echo "${FRAMEWORKPATH}" | tr ':' ' ' ) -maxdepth 1 -mindepth 0 -name 'framework.sh' 2>/dev/null )"
while read flocation ; do if test -e ${flocation} ; then __thisfver="$( sh ${flocation} --fcheck 2>/dev/null )" ; if test ${__thisfver:-0} -ge ${f_needed} ; then frameworkscript="${flocation}" ; break; elif test -n "${___thisfver}" ; then printf "Obsolete: %s %s\n" "${flocation}" "${__thisfver}" 1>&2 ; fi ; fi ; done <<EOFLOCATIONS
${FRAMEWORKBIN:-/bin/false}
${___frameworkpath:-/bin/false}
./framework.sh
${scriptdir}/framework.sh
$HOME/bin/bgscripts/framework.sh
$HOME/bin/framework.sh
$HOME/bgscripts/framework.sh
$HOME/framework.sh
$HOME/.local/share/bgscripts/framework.sh
/usr/local/bin/bgscripts/framework.sh
/usr/local/bin/framework.sh
/usr/bin/bgscripts/framework.sh
/usr/bin/framework.sh
/bin/bgscripts/framework.sh
/usr/local/share/bgscripts/framework.sh
/usr/share/bgscripts/framework.sh
/usr/libexec/bgscripts/framework.sh
EOFLOCATIONS
test -z "${frameworkscript}" && echo "$0: framework ${f_needed} not found. Try setting FRAMEWORKPATH. Aborted." 1>&2 && exit 4

# INITIALIZE VARIABLES
# variables set in framework:
# today server thistty scriptdir scriptfile scripttrim
# is_cronjob stdin_piped stdout_piped stderr_piped sendsh sendopts
. ${frameworkscript} || echo "$0: framework did not run properly. Continuing..." 1>&2
infile1=
outfile1=
logfile=${scriptdir}/${scripttrim}.${today}.out
define_if_new interestedparties "bgstack15@gmail.com"
# SIMPLECONF
define_if_new default_conffile "/etc/addgrouptogroup/addgrouptogroup.conf"
define_if_new defuser_conffile ~/.config/addgrouptogroup/addgrouptogroup.conf
define_if_new AGTG_TMPDIR "$( mktemp -d )"
AGTG_TMPFILE_ETC_GROUP="$( TMPDIR="${AGTG_TMPDIR}" mktemp )"
#tmpfile2="$( TMPDIR="${AGTG_TMPDIR}" mktemp )"

# REACT TO OPERATING SYSTEM TYPE
case $( uname -s ) in
   Linux) : ;;
   FreeBSD) : ;;
   *) echo "${scriptfile}: 3. Indeterminate OS: $( uname -s )" 1>&2 && exit 3;;
esac

# REACT TO ROOT STATUS
case ${is_root} in
   1) # proper root
      : ;;
   sudo) # sudo to root
      : ;;
   "") # not root at all
      ferror "${scriptfile}: 5. Please run as root or sudo. Aborted."
      exit 5
      ;;
esac

# SET CUSTOM SCRIPT AND VALUES
#setval 1 sendsh sendopts<<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
#/usr/local/share/bgscripts/send.sh -hs  # setvalout maybe be "fail" otherwise
#/usr/share/bgscripts/send.sh -hs        # on success, setvalout="valid-sendsh"
#/usr/local/bin/send.sh -hs
#/usr/bin/mail -s
#EOFSENDSH
#test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. mailer not found. Aborted." && exit 4

# VALIDATE PARAMETERS
# objects before the dash are options, which get filled with the optvals
# to debug flags, use option DEBUG. Variables set in framework: fallopts
validateparams - "$@"

# LEARN EX_DEBUG
test -z "${__debug_set_by_param}" && fisnum "${AGTG_DEBUG}" && debug="${AGTG_DEBUG}"

# CONFIRM TOTAL NUMBER OF FLAGLESSVALS IS CORRECT
#if test ${thiscount} -lt 2;
#then
#   ferror "${scriptfile}: 2. Fewer than 2 flaglessvals. Aborted."
#   exit 2
#fi

# LOAD CONFIG FROM SIMPLECONF
# This section follows a simple hierarchy of precedence, with first being used:
#    1. parameters and flags
#    2. environment
#    3. config file
#    4. default user config: ~/.config/script/script.conf
#    5. default config: /etc/script/script.conf
if test -f "${conffile}";
then
   get_conf "${conffile}"
else
   if test "${conffile}" = "${default_conffile}" || test "${conffile}" = "${defuser_conffile}"; then :; else test -n "${conffile}" && ferror "${scriptfile}: Ignoring conf file which is not found: ${conffile}."; fi
fi
test -f "${defuser_conffile}" && get_conf "${defuser_conffile}"
test -f "${default_conffile}" && get_conf "${default_conffile}"

# CONFIGURE VARIABLES AFTER PARAMETERS
define_if_new AGTG_ETC_GROUP "/etc/group"
define_if_new AGTG_ACTION add # options are add or remove
# define_if_new AGTG_TARGET_GROUP # no default. It must be defined.

if test -z "${AGTG_TARGET_GROUP}" ;
then
   ferror "${scriptfile}: 2 fatal. Must define --target <value> or AGTG_TARGET_GROUP=value. Aborted."
   exit 2
fi

## REACT TO BEING A CRONJOB
#if test ${is_cronjob} -eq 1;
#then
#   :
#else
#   :
#fi

# SET TRAPS
#trap "CTRLC" 2
#trap "CTRLZ" 18
trap '__ec=$? ; clean_addgrouptogroup ; trap "" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20 ; exit ${__ec} ;' 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20

# DEBUG SIMPLECONF
debuglev 5 && {
   ferror "Using values"
   # used values: EX_(OPT1|OPT2|VERBOSE)
   set | grep -iE "^AGTG_" 1>&2
}

# Prepare backup copy of /etc/group file so we can compare at the end
/bin/cp -p "${AGTG_ETC_GROUP}" "${AGTG_TMPFILE_ETC_GROUP}"

# MAIN LOOP
#{

   x=0
   while test $x -lt $thiscount ;
   do
      x=$(( x + 1 ))
      eval AGTG_this_group="\${opt${x}}"
      debuglev 7 && ferror "Evaluating group \"${AGTG_this_group}\""
      case "${AGTG_ACTION}" in
         add|remove)
            workflow_${AGTG_ACTION} "${AGTG_TARGET_GROUP}" "${AGTG_this_group}" "${AGTG_ETC_GROUP}"
            ;;
      esac
   done

   # Check to see if any changes occurred. If not, close normally.
   # if files are same, diff -q file1 file2 1>/dev/null 2>&1  RC=0
   if diff -q "${AGTG_ETC_GROUP}" "${AGTG_TMPFILE_ETC_GROUP}" 1>/dev/null 2>&1 ;
   then
      debuglev 1 && ferror "no changes"
   else
      echo "changed"
   fi

   :

#} | tee -a ${logfile}

# EMAIL LOGFILE
#${sendsh} ${sendopts} "${server} ${scriptfile} out" ${logfile} ${interestedparties}
