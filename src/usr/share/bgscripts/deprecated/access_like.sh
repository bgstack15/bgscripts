#!/bin/sh
# Filename: access_like.sh
# Location: /usr/libexec/bgscripts/
# Author: bgstack15@gmail.com
# Startdate: 2018-06-06 14:58:41
# Title: Script That Sets Access Like A User For a Different User
# Purpose: To make it easy to set up similar user access
# Package:
# History:
#    2018-02-01 Initial ansible playbook written
#    2018-06-06 shell script written
#    2018-12-10 change directory
# Usage:
# Reference: ftemplate.sh 2018-05-15m; framework.sh 2017-11-11m
#    access_like.yml
# Improve:
# Dependencies:
#    userinfo.sh
# Documentation:
#  This script performs several major functions:
#    Learn if users are local or domain
#    If both local, set up local group memberships to be identical, except for user private groups
#    If ssh uses AllowUsers, make thisuser match likeuser
fiversion="2018-05-15m"
access_likeversion="2018-06-08a"

usage() {
   ${PAGER:-/usr/bin/less -F} >&2 <<ENDUSAGE
usage: access_like.sh [-duV] [-n] [-c conffile] [-l <likeuser>] <thisuser1> [ <thisuser2> ... ]
version ${access_likeversion}
 -d debug   Show debugging info, including parsed variables.
 -u usage   Show this usage block.
 -V version Show script version number.
 -c conf    Read in this config file.
 -l likeuser  Set AL_LIKEUSER
 -n dryrun  Set AL_DRYRUN
 -a apply   Unset AL_DRYRUN
 -h --nh    Set or unset AL_HANDLERS
Environment variables:
AL_LIKEUSER   Match the permissions and access of this user
AL_DEBUG [0-10]  Set debug value to a number, like -d flag.
AL_DRYRUN     If truthy, do not apply changes. Just report.
AL_HANDLERS   If truthy, the script will restart affected daemons: sshd, sssd. Default is 1.
Return values:
 0 Normal
 1 Help or version info displayed
 2 Count or type of flaglessvals is incorrect
 3 Incorrect OS type
 4 Unable to find dependency
 5 Not run as root or sudo
 6 Likeuser is invalid
Debug levels:
 0 Normal operation
 1 Show commands run
 2 Show tasks skipped because they are already compliant
 4 Show changes made to config files
 5 Show all input AL_ variables
ENDUSAGE
}

# DEFINE FUNCTIONS
add_user_to_group() {
   #call: "${thisuser}" "${group}" "${these_groups_already}" "${dryrun}"
   local thisuser="${1}"
   local thisgroup="${2}"
   local these_groups_already="${3}"
   local dryrun="${4}"

   if fistruthy "${dryrun}" ;
   then
      echo /usr/bin/gpasswd -a "${thisuser}" "${thisgroup}"
   else
      # only if user is not in this group already
      if echo "${these_groups_already}" | grep -qx "${thisgroup}" ;
      then
         debuglev 2 && ferror "${thisuser} already in group ${thisgroup}"
      else
         debuglev 1 && ferror "/usr/bin/gpasswd -a ${thisuser} ${thisgroup}"
         /usr/bin/gpasswd -a "${thisuser}" "${thisgroup}" | grep -vE 'Adding user.*to group.*'
         AL_CHANGED=$(( AL_CHANGED + 1 ))
      fi
   fi

}

access_like() {
   # call: access_like "${likeuser}" "${thisuser}" "${dryrun}"
   debuglev 9 && ferror "access_like $@"
   local likeuser="${1}"
   local thisuser="${2}"
   local dryrun="${3}"

   # LEARN IF USERS ARE LOCAL
   local getent_local="$( getent passwd -s files "${likeuser}" "${thisuser}" 2>/dev/null )"
   local getent_domain="$( getent passwd -s sss "${likeuser}" "${thisuser}" 2>/dev/null )"

   local likeuser_is_local="$( echo "${getent_local}" | grep -qE "^${likeuser}" && echo "1" )"
   local likeuser_is_domain="$( echo "${getent_domain}" | grep -qE "^${likeuser}" && echo "1" )"
   local thisuser_is_local="$( echo "${getent_local}" | grep -qE "^${thisuser}" && echo "1" )"
   local thisuser_is_domain="$( echo "${getent_domain}" | grep -qE "^${thisuser}" && echo "1" )"

   ## LOCAL GROUPS

   # learn groups of local likeuser excluding user private group
   local thesegroups="$( awk -F':' "/:.*\<${likeuser}\>/ {print \$1}" "${AL_GROUP_FILE}" 2>/dev/null )"

   # learn primary group of first user
   local this_primary_group="$( /usr/bin/id -ng "${likeuser}" 2>/dev/null )"

   # add thisuser to thesegroups
   local these_groups_already="$( /usr/bin/id -G "${thisuser}" 2>/dev/null | xargs /usr/bin/getent group 2>/dev/null | /usr/bin/awk -F':' '{print $1}' )"
   for word in ${thesegroups} ;
   do
      add_user_to_group "${thisuser}" "${word}" "${these_groups_already}" "${dryrun}"
   done

   # set thisuser to user private group, if user private group
   if test -n "${thisuser_is_local}" ;
   then
      if echo "${this_primary_group}" | grep -qE "${likeuser}" ;
      then
         add_user_to_group "${thisuser}" "${thisuser}" "${these_groups_already}" "${dryrun}"
      else
         # add thisuser to this_primary_group if not user private group
         add_user_to_group "${thisuser}" "${this_primary_group}" "${these_groups_already}" "${dryrun}"
      fi
   fi

   ## SSH and SSSD

   # learn if ssh uses AllowUsers
   local ssh_uses_allowusers="$( grep -qiE "^\s*AllowUsers" "${AL_SSHD_CONFIG_FILE}" && echo 1 )"

   # learn if sssd uses simple_allow_users
   local sssd_uses_simple_allow_users="$( grep -qie "^\s*simple_allow_users" "${AL_SSSD_CONF_FILE}" && echo 1 )"

   # if ssh uses AllowUsers
   if test "${ssh_uses_allowusers}" = "1" ;
   then
      # learn if likeuser can ssh
      local likeuser_can_ssh="$( grep -qiE "^\s*AllowUsers.*\<${likeuser}\>" "${AL_SSHD_CONFIG_FILE}" && echo 1 )"

      # learn if thisuser can already ssh
      local thisuser_can_ssh="$( grep -qiE "^\s*AllowUsers.*\<${thisuser}\>" "${AL_SSHD_CONFIG_FILE}" && echo 1 )"
   fi

   # if sssd uses simple_allow_users
   if test "${sssd_uses_simple_allow_users}" = "1" ;
   then
      # learn if likeuser can sssd
      local likeuser_can_sssd="$( grep -qiE "^\s*simple_allow_users.*\<${likeuser}\>" "${AL_SSSD_CONF_FILE}" && echo 1 )"

      # learn if thisuser can already sssd
      local thisuser_can_sssd="$( grep -qie "^\s*simple_allow_users.*\<${thisuser}\>" "${AL_SSSD_CONF_FILE}" && echo 1 )"
   fi

   ## SSH
   debuglev 4 && AL_MODCONF_VERBOSE="-v"
   # add thisuser to ssh AllowUsers, if likeuser can ssh but thisuser cannot
   if test "${likeuser_can_ssh}" = "1" && test "${thisuser_can_ssh}" != "1" ;
   then
      "${AL_PYTHON}" "${AL_MODCONF}" ${AL_MODCONF_VERBOSE} -a "${AL_SSHD_CONFIG_FILE}" --itemdelim " " --variabledelim " " add AllowUsers "${thisuser}" && \
      AL_CHANGED=$(( AL_CHANGED + 1 )) && \
      AL_NOTIFY="${AL_NOTIFY+${AL_NOTIFY},}sshd"
   fi

   ## SSSD
   # add thisuser to sssd simple_allow_users, if likeuser can sssd but thisuser cannot
   if test "${likeuser_can_sssd}" = "1" && test "${thisuser_can_sssd}" != "1" ;
   then
      "${AL_PYTHON}" "${AL_MODCONF}" ${AL_MODCONF_VERBOSE} -a "${AL_SSSD_CONF_FILE}" --itemdelim ", " --variabledelim " " add simple_allow_users "${thisuser}" && \
      AL_CHANGED=$(( AL_CHANGED + 1 )) && \
      AL_NOTIFY="${AL_NOTIFY+${AL_NOTIFY},},sssd"
   fi

}

# DEFINE TRAPS

clean_access_like() {
   # use at end of entire script if you need to clean up tmpfiles
   # rm -f "${tmpfile1}" "${tmpfile2}" 2>/dev/null

   # Delayed cleanup
   if test -z "${AL_NO_CLEAN}" ;
   then
      nohup /bin/bash <<EOF 1>/dev/null 2>&1 &
sleep "${AL_CLEANUP_SEC:-3}" ; /bin/rm -r "${AL_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

CTRLC() {
   # use with: trap "CTRLC" 2
   # useful for controlling the ctrl+c keystroke
   :
}

CTRLZ() {
   # use with: trap "CTRLZ" 18
   # useful for controlling the ctrl+z keystroke
   :
}

parseFlag() {
   flag="$1"
   hasval=0
   case ${flag} in
      # INSERT FLAGS HERE
      "d" | "debug" | "DEBUG" | "dd" ) setdebug; ferror "debug level ${debug}";;
      "u" | "usage" | "help" | "h" ) usage; exit 1;;
      "V" | "fcheck" | "version" ) ferror "${scriptfile} version ${access_likeversion}"; exit 1;;
      #"i" | "infile" | "inputfile" ) getval; infile1=${tempval};;
      "c" | "conf" | "conffile" | "config" ) getval; conffile="${tempval}";;
      "l" | "likeuser" | "like-user" | "like" ) getval; AL_LIKEUSER="${tempval}";;
      "n" | "dryrun" | "noapply" ) AL_DRYRUN=1 ;;
      "a" | "apply" ) unset AL_DRYRUN ;;
      "h" | "handler" | "handlers" ) AL_HANDLERS=1 ;;
      "nh" | "nohandler" | "nohandlers" | "no-handler" | "no-handlers" ) AL_HANDLERS=0 ;;
   esac

   debuglev 10 && { test ${hasval} -eq 1 && ferror "flag: ${flag} = ${tempval}" || ferror "flag: ${flag}"; }
}

# DETERMINE LOCATION OF FRAMEWORK
f_needed=20171111
while read flocation ; do if test -e ${flocation} ; then __thisfver="$( sh ${flocation} --fcheck 2>/dev/null )" ; if test ${__thisfver} -ge ${f_needed} ; then frameworkscript="${flocation}" ; break; else printf "Obsolete: %s %s\n" "${flocation}" "${__this_fver}" 1>&2 ; fi ; fi ; done <<EOFLOCATIONS
./framework.sh
${scriptdir}/framework.sh
$HOME/bin/bgscripts/framework.sh
$HOME/bin/framework.sh
$HOME/bgscripts/framework.sh
$HOME/framework.sh
/usr/local/bin/bgscripts/framework.sh
/usr/local/bin/framework.sh
/usr/bin/bgscripts/framework.sh
/usr/bin/framework.sh
/bin/bgscripts/framework.sh
/usr/local/share/bgscripts/framework.sh
/usr/share/bgscripts/framework.sh
/usr/libexec/bgscripts/framework.sh
EOFLOCATIONS
test -z "${frameworkscript}" && echo "$0: framework not found. Aborted." 1>&2 && exit 4

# INITIALIZE VARIABLES
# variables set in framework:
# today server thistty scriptdir scriptfile scripttrim
# is_cronjob stdin_piped stdout_piped stderr_piped sendsh sendopts
. ${frameworkscript} || echo "$0: framework did not run properly. Continuing..." 1>&2
infile1=
outfile1=
logfile=${scriptdir}/${scripttrim}.${today}.out
define_if_new interestedparties "bgstack15@gmail.com"
# SIMPLECONF
define_if_new default_conffile "/tmp/access_like.conf"
define_if_new defuser_conffile ~/.config/access_like/access_like.conf
define_if_new AL_TMPDIR "$( mktemp -d )"
#tmpfile1="$( TMPDIR="${AL_TMPDIR}" mktemp )"
#tmpfile2="$( TMPDIR="${AL_TMPDIR}" mktemp )"
define_if_new AL_SSHD_CONFIG_FILE /etc/ssh/sshd_config
define_if_new AL_SSSD_CONF_FILE /etc/sssd/sssd.conf
define_if_new AL_GROUP_FILE /etc/group
define_if_new AL_SUDOERS_FILE /etc/sudoers
define_if_new AL_SUDOERS_DIR /etc/sudoers.d

# REACT TO OPERATING SYSTEM TYPE
case $( uname -s ) in
   Linux) : ;;
   FreeBSD) : ;;
   *) echo "${scriptfile}: 3. Indeterminate OS: $( uname -s )" 1>&2 && exit 3;;
esac

# SET CUSTOM SCRIPT AND VALUES
#setval 1 sendsh sendopts<<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
#/usr/local/share/bgscripts/send.sh -hs  # setvalout maybe be "fail" otherwise
#/usr/share/bgscripts/send.sh -hs        # on success, setvalout="valid-sendsh"
#/usr/local/bin/send.sh -hs
#/usr/bin/mail -s
#EOFSENDSH
#test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. mailer not found. Aborted." && exit 4

setval 1 AL_PYTHON <<EOFPYTHON
/usr/bin/python2
$( which python2 2>/dev/null )
EOFPYTHON
test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. python2 not found. Aborted." && exit 4

setval 1 AL_MODCONF <<EOFMODCONF
/usr/share/bgscripts/py/modconf.py
/usr/libexec/bgscripts/py/modconf.py
/tmp/modconf.py
EOFMODCONF
test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. modconf.py not found. Aborted." && exit 4

# VALIDATE PARAMETERS
# objects before the dash are options, which get filled with the optvals
# to debug flags, use option DEBUG. Variables set in framework: fallopts

validateparams - "$@"

# REACT TO ROOT STATUS
# moved to after validateparams so it will do the --help without sudo
case ${is_root} in
   1) # proper root
      : ;;
   sudo) # sudo to root
      : ;;
   "") # not root at all
      ferror "${scriptfile}: 5. Please run as root or sudo. Aborted."
      exit 5
      :
      ;;
esac

# CONFIRM TOTAL NUMBER OF FLAGLESSVALS IS CORRECT
#if test ${thiscount} -lt 2;
#then
#   ferror "${scriptfile}: 2. Fewer than 2 flaglessvals. Aborted."
#   exit 2
#fi
# fail out if likeuser is not defined
if test -z "${AL_LIKEUSER}" || test -z "$( getent passwd "${AL_LIKEUSER}" 2>/dev/null )" ;
then
   ferror "Fatal: 6. Unable to determine likeuser \"${AL_LIKEUSER}\". Aborted."
   exit 6
fi


# LOAD CONFIG FROM SIMPLECONF
# This section follows a simple hierarchy of precedence, with first being used:
#    1. parameters and flags
#    2. environment
#    3. config file
#    4. default user config: ~/.config/script/script.conf
#    5. default config: /etc/script/script.conf
if test -f "${conffile}";
then
   get_conf "${conffile}"
else
   if test "${conffile}" = "${default_conffile}" || test "${conffile}" = "${defuser_conffile}"; then :; else test -n "${conffile}" && ferror "${scriptfile}: Ignoring conf file which is not found: ${conffile}."; fi
fi
test -f "${defuser_conffile}" && get_conf "${defuser_conffile}"
test -f "${default_conffile}" && get_conf "${default_conffile}"

# CONFIGURE VARIABLES AFTER PARAMETERS
test -n "${AL_DEBUG}" && test ${debug} = "0" && debug="${AL_DEBUG}"
test -z "${AL_HANDLERS}" && AL_HANDLERS=1

## REACT TO BEING A CRONJOB
#if test ${is_cronjob} -eq 1;
#then
#   :
#else
#   :
#fi

# SET TRAPS
#trap "CTRLC" 2
#trap "CTRLZ" 18
trap "__ec=$? ; clean_access_like ; trap '' 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20 ; exit ${__ec} ;" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18 19 20

# DEBUG SIMPLECONF
debuglev 5 && {
   ferror "Using values"
   # used values: EX_(OPT1|OPT2|VERBOSE)
   set | grep -iE "^AL_" 1>&2
}

# MAIN LOOP
#{

   # report on dryrun
   fistruthy "${AL_DRYRUN}" && echo "DRY RUN ONLY!"

   x=0
   AL_CHANGED=0
   while test ${x:-0} -lt "${thiscount:-0}" ;
   do
      x=$(( x + 1 ))
      eval AL_THISUSER="\${opt${x}}"
      access_like "${AL_LIKEUSER}" "${AL_THISUSER}" "${AL_DRYRUN}"
   done

   ## SUDOERS is outside the loop because this script only accepts one likeuser.

   # learn if likeuser is in sudoers
   in_sudoers="$( grep -rE "\<${AL_LIKEUSER}\>" "${AL_SUDOERS_FILE}" "${AL_SUDOERS_DIR}" 2>/dev/null )"

   # check sudoers on these hosts
   if echo "${in_sudoers}" | grep -qE "\<${AL_LIKEUSER}\>" ;
   then
      echo "${server} WARNING: Check sudoers manually"
      grep -rE "\<${AL_LIKEUSER}\>" "${AL_SUDOERS_FILE}" "${AL_SUDOERS_DIR}" 2>/dev/null | sed -r -e "s/^/${server} /;"

   fi

   # handlers
   if fistruthy "${AL_HANDLERS}" ;
   then
      if echo "${AL_NOTIFY}" | grep -qE "sshd" ;
      then
         if debuglev 1 ;
         then
            /usr/sbin/service sshd restart 1>&2 ;
         else
            /usr/sbin/service sssd restart 1>/dev/null 2>&1 ;
         fi
      fi
      if echo "${AL_NOTIFY}" | grep -qE "sssd" ;
      then
         if debuglev 1 ;
         then
            /usr/sbin/service sssd restart 1>&2 ;
         else
            /usr/sbin/service sssd restart 1>/dev/null 2>&1 ;
         fi
      fi
   fi

#} | tee -a ${logfile}

# EMAIL LOGFILE
#${sendsh} ${sendopts} "${server} ${scriptfile} out" ${logfile} ${interestedparties}

# exit for ansible
test "${AL_CHANGED}" != "0" && echo "changed"
exit 0
