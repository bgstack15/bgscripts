title txt2man-wrapper
section 1
project bgscripts-core
volume General Commands Manual
date April 2020
=====
NAME
  txt2man-wrapper - wrap around txt2man to add heading support
SYNOPSIS
  txt2man-wrapper [- | <file>]
OPTIONS
  -       Use standard input. Default if no filename provided.
  <file>  Use this file.
ENVIRONMENT VARIABLES
  INFILE  Use this file. Takes precedence over input parameter.
  USE_STDIN  If "1", then use standard input. Takes precedent over everything else.
DESCRIPTION
  Add various heading support to `txt2man(1)`. Program `txt2man(1)` requires parameters on the command line for the volume name, section number, page name, et al.

  This program adds such support, all from the file, so the command line invocation needs no additional parameters.
FILE FORMAT
  For a text file to be given all supported headings, the top of the file must contain these entries, followed by a line filled with at least 5 equal signs `=`.

    title txt2man-wrapper
    section 1
    project bgscripts-core
    volume General Commands Manual
    date April 2020
    =====
    name(but in capitals)
AUTHOR
  Ben Stack <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
SEE ALSO
`txt2man(1)`
