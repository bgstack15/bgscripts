title sizer
section 1
project bgscripts-core
volume General Commands Manual
date April 2020
=====
NAME
  sizer - summarize directory contents by file extension
SYNOPSIS
  sizer [<path>]
OPTIONS
  <path> Directory to summarize. If omitted, will use $PWD.
DESCRIPTION
  Find all files underneath provided path and display aggregate size grouped by file extension.
ENVIRONMENT
* SIZER_SORT  If not null, pass to `sort -n`.
* SIZER_HUMAN  If not null, use human-readable numbers. (KB, MB, GB, and not KiB, MiB, GiB).
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
