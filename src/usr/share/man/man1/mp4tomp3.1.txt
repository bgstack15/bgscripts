title mp4tomp3
section 1
project bgscripts
date April 2020
volume General Commands Manual
=====
NAME
  mp4tomp3 - convert mp4 files to mp3 with best possible encodings
SYNOPSIS
  mp4tomp3 [-d {0..10}] [-uV] <file1> [<file2> ...]
OPTIONS
  -d {0..10}  Set debug level, with 10 as maxium verbosity.
  -u --usage  Show help message.

  -V --version Show version number.
  <filenames>  Convert these files.
DESCRIPTION
  Convert provided files, using `ffmpeg(1)` or `avconv(1)` (whichever it finds first), from mp4 to mp3 format. Find closest matching bitrate from a list of presets.
BUGS
* Not very robust.
* Not the preferred tool of author now. Now he uses `youtube-dl(1)` with various flags.
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
SEE ALSO
`ffmpeg(1)`, `avconv(1)`
