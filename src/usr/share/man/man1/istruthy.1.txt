title istruthy
section 1
project bgscripts-core
volume General Commands Manual
date April 2020
=====
NAME
  istruthy - return true if $1 is any category of true
SYNOPSIS
  istruthy <value>
OPTIONS
  <value>   Any input.
DESCRIPTION
  Return true if the loewrcase <value> is any of the following values:
* yes
* 1
* y
* true
* always
* on
HISTORY
  This is the same as function `fistruthy` from `framework.sh(7)` library.
SEE ALSO
`framework.sh(7)`
